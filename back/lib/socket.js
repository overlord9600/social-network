const model = require('./model')

class Socket{
    constructor(socket) {
      this.socket=socket


      this.socket.on('startChat',(data)=> this.findeChat(data))

      this.socket.on('newMessage',(data)=> this.addMessage(data))

     }
    findeChat(data){
        model.findAll('users',{token:data.me})
             .then(r=>{
                 if(r.length>0){
                     r=r[0]
                     return model.findeChat('user1_id','user2_id',r.id,data.id)
                 }
             })
             .then(r=>{
                 this.socket.emit('getMessages',r);
             })
    }

    addMessage(data){
        let myId
        model.findAll('users',{token:data.token})
        .then(r=>{
            if(r.length>0){
                r=r[0]
                myId=r.id
                // return console.log(data);
               return model.add('chat',{user1_id:r.id,user2_id:data.friend,text:data.text,status:0})
            }
        })
        .then(r=>{
            model.findeChat('user1_id','user2_id',myId,data.friend)
                 .then(rr=>{
                    this.socket.broadcast.emit('newMessage',rr)
                    this.socket.emit('newMessage',rr)
                 })
            
        })
    }

    
}

module.exports = Socket