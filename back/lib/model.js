const mysql = require('mysql');
const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'social_vahan1'
});

connection.connect();

module.exports = {
    friendRequest:(table,rFriend)=> {
        return new Promise((resolve, reject) => {
            connection.query(`SELECT * FROM users
             where id in (SELECT user1 
                FROM ${table} WHERE user2=${rFriend})`, (err, data) => {
                if (err) reject(err);
                resolve(data);
            });
        });
    },
    
    //login
    findAll: (table, obj)=>{
        return new Promise((resolve, reject) => {
            let command = `SELECT * FROM ${table} where `
            for(let key in obj){
                command += `${key} = '${obj[key]}' and `
            }
            command = command.substring(0, command.length-4)
            
            connection.query(command, (err, data) => {
                if (err) reject(err);
                resolve(data);
            });
        });
    },
    //adminfindall
    adminfindall: (table)=>{
        return new Promise((resolve,reject)=>{
             connection.query(`select * from ${table}`,(err,data)=>{
                if (err) reject(err);
                resolve(data);
             })
        })
    },

    // //find friend request
    // findeRequest:(table,column,obj)=>{
    //     return new Promise((resolve,reject)=>{
    //         let command = `SELECT * FROM ${table} WHERE ${column} = ${obj}`
             
    //         connection.query(command, (err,data)=>{
    //             if(err) reject(err);
    //             resolve(data)
    //         })
    //     })
    // },
 
    //register
    add:(table, obj)=> {
        return new Promise((resolve, reject) => {
            let h = 'INSERT INTO ' + table + '('
            for (let key in obj) {
                h += key + ','
            }
            h = h.substring(0, h.length - 1) + ')'

            h += ' values('
            for (let key in obj) {
                h += `'${obj[key]}',`
            }
            h = h.substring(0, h.length - 1) + ')';

            connection.query(h, (err, data) => {
                if(err) reject(err);    // h
                resolve(data);
            });
        });
    },

    remove:(table, obj)=> {
        return new Promise((resolve, reject) => {
            let d = 'DELETE FROM ' + table
            d += ' WHERE'
            for (let key in obj) {
                d += ` ${key} = '${obj[key]}' and `
            }
            d = d.substring(0, d.length - 4);
            connection.query(d, (err, data) => {
                if (err) reject(err);   // d
                resolve(data);
            })
        })
    },

    //change password or login
    update: (table, obj, obj1)=> {
        return new Promise((resolve, reject) => {
            let query = 'UPDATE ' + table
            query += ' set'
            for (let key1 in obj1) {
                query += ` ${key1} = '${obj1[key1]}',`
            }
            //update users set 
            query = query.substring(0, query.length - 1);

            query += ' WHERE'
            for (let key in obj) {
                query += ` ${key} = '${obj[key]}' and `
            }
            query = query.substring(0, query.length - 4);
            // console.log(query);
            connection.query(query, (err, data) => {
                if (err) reject(err);   // query
                resolve(data);
            });
        });
    },

    //error count
    updateQuantity:(id)=>{
        return new Promise((resolve,reject)=>{
            let query = `UPDATE users set quantity = quantity-1 where id=${id}`
            // console.log(query);
            connection.query(query,(err,data)=>{
                if(err)reject(err)
                resolve(data)
                // console.log(data.message);
                // console.log(data.serverStatus);
            })
        })
    },


    //search sistem
    searchUser: (table,text)=>{
        return new Promise((resolve,reject)=>{
            let command=`select * from ${table} where name like '${text}%'`
            connection.query(command, (err,data)=>{
                if(err) reject(err);
                return resolve(data)
            })
        })
    },

    findeFriends:(table1,table2,id)=>{
        return new Promise((resolve,reject)=>{
            connection.query(`select * from ${table1} where id in(
                select user1 from ${table2} where user2 = ${id}
                UNION
                select user2 from ${table2} where user1 = ${id}
                )`,(err,data)=>{
                    if(err) reject(err);
                    return resolve(data)
                })
        })
    },

     //select * from chat
//where (user1_id = 10 and user2_id = 11) or (user1_id = 11 and user2_id =10)
    findeChat:(column1, column2, id1, id2)=>{
        return new Promise((resolve,reject)=>{
            let order = `select * from chat where (${column1}= ${id1} and ${column2}=${id2}) or (${column1}= ${id2} and ${column2}=${id1})`
            connection.query(order,(err,data)=>{
                if(err) reject(err)
                resolve(data)
            })
        })
    }
}

