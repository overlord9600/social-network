const model = require('../lib/model')

const uniqid = require("uniqid");

const bcrypt = require('bcrypt');
const saltRounds = 10;
class UserController{

    add(req,res){
        
        delete req.body.error
        model.findAll('users',{login:req.body.login})
             .then(r=>{
                // return console.log(req.body);

                const regp="^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{6,})";
                const regm = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
              
            if(!req.body.login||!req.body.password||!req.body.name||!req.body.surname){
                for(let key in req.body){
                    if(!req.body[key]){
                        res.send({error:`${key} is empty`})
                        break
                    }
                } 
            }else if(!req.body.password.match(regp)){
                     res.send({error:'password must contain min. 1 simbol, 1 number and 1 uppercase letter'})
                 }else if(!req.body.login.match(regm)){
                     res.send({error:'not valid login'})
                 }else{
                     if(r.length===0){
                         bcrypt.hash(req.body.password, saltRounds, function(err, hash) {
                            if(err) throw err;
                            req.body.password = hash;
                            req.body.token = uniqid();
                            model.add('users',{login:req.body.login,password:req.body.password,name:req.body.name,surname:req.body.surname,photo:'avatar.png'})
                            res.send({success:'Your account was successfully registered'})
                         });
                     }else{
                         res.send({error:'This login is busy, choose another one!!!'})
                 
                      }
                    }
                    
                      
                 
                
             })
    }

    login(req,res){
        delete req.body.error
        
    const log=req.body.login
 
    model.findAll('users',{login:log})
         .then(r=>{
            for(let key in req.body){
                if(!req.body[key]){
                    res.send({error:`${key} is empty`})
                    break
                }else if(r.length == 0){
                 res.send({error:"Login is wrong"})
             }else{
                 r = r[0];
     
                 bcrypt.compare(req.body.password, r.password, (err, result)=> {
                   if(err) throw err;
                   let count=r.quantity
                   let time = Date.now()/1000

                   if(!result){
                        if(count<3){
                            count++
                            model.update('users',{id:r.id},{quantity:count})
                        }else{
                            model.update('users',{id:r.id},{block:time})
                               
                            return res.send({error:`YOU ARE BLOCKED, TRY AFTER ${parseInt(time-r.block)} MINUTES`})
                        }

                        res.send({error:"Password is wrong"})
                               
                   }else if(time > 3){

                        let tok = r.name.split("").join("~")+r.id;
                        let time = (Date.now()/1000)-r.block
                        // console.log(time)
                        
                            model.update("users",{id:r.id}, {time:Date.now()/1000, token:tok, block:0, quantity:0})
                            .then(rr=>{
                                if(r.status===1){
                                    res.send({error:'Admin blocked your account'})
                                }else if(r.type==0){
                                    res.send({success:"You've been signed in successfully", token:tok})
                                }else if(r.type==1){
                                 res.send({adminsuccess:"You've been signed in successfully", token:tok})
                                }
                            })
                        }else{
                            return res.send({error:"YOU ARE BLOCKED!!!!!"})

                        }
                });
             }

             break
            }
         })

    }

    usersForAdmin(req,res){
        model.adminfindall('users')
         .then(r=>{
             res.send(r);
         })
    }

    loadProfile(req,res){
        let token=req.body.token
        model.findAll('users',{token})
             .then(async r=>{
                if(r.length==0){
                    res.send({error:'not such account'})
                }else{
                    r=r[0]
                    let now=Date.now()/1000
                    let timeOut=(now-r.time)
                    if(timeOut>200){
                        model.update("users",{id:r.id},{token:"", time:0})
                             .then(r=>{
                                 res.send({error:"Time limit error, please log in again"})
                             })
                        return;
                    }
                         let token = uniqid();
                       await model.update("users",{ id: r.id }, {time: Math.floor(Date.now() / 1000), token: token})
                            .then(a => {
                                res.send({ user: r, success: "You have been loged in successfully", token: token });
                            })
                   
                    
                }
            })
    }

    changeLogin(req,res){
         let token=req.body.token
        let login=req.body.login
        let password=req.body.password
        model.findAll('users',{token:token})
             .then(r=>{
                 if(!login||!password){
                    for(let key in req.body){
                        if(!req.body[key]){
                           res.send({error:`${key} is empty`})
                           break
                        }
                     }
                 }else if(r.length==0){
                      res.send({error:'no such user'})
                 }else{
                     r=r[0]
                     bcrypt.compare(password,r.password, (err,data)=>{
                        if(err) throw err;
                        if(!data){
                             res.send({error:'wrong password'})
                        }else{
                            model.update("users",{id:r.id}, {login:req.body.login})
                            .then(rr=>{
                                 res.send({success:"Your email was changed"})
                             })
                        }
                     })
                 }
              })
    }

    changePwd(req,res){
        let token = req.body.token
    
    model.findAll('users',{token:token})
         .then(r=>{
            if(!req.body.newpassword||!req.body.oldpassword){
                for(let key in req.body){
                    if(!req.body[key]){
                       res.send({error:`${key} is empty`})
                       break
                    }
                }
            }else if(r.length===0){
                             res.send({error:'no such user'})
                         }else{
                             r=r[0]
                             
                             bcrypt.compare(req.body.oldpassword, r.password, (err,data)=>{
                                 if(err) throw err;
                                 if(!data){
                                     res.send({error:'wrong password'})
                                 }else{
                                     bcrypt.hash(req.body.newpassword,saltRounds, (err,hash)=>{
                                        model.update('users',{id:r.id},{password:hash})
                                            .then(rr=>{
                                                res.send({success:'Your password was changed'})
                                            })
                                     })
                                
                                 }
                             })
                         }
                        })
                }

    searchSis(req,res){
        model.searchUser('users',req.body.text)
        .then(r=>{
            res.send(r)
         })
    }

    request(req,res){
        let profileOwnre = req.body.myToken
        let requestedFriend = req.body.id
           model.findAll('users',{token:profileOwnre})
               .then(r1=>{
                  r1=r1[0]
     
                  model.add("requests", {user1:r1.id, user2:requestedFriend})
                       .then(r=>{
                            res.send({success:"OK"})
                        })
             })
    }

    loadRequests(req,res){
        model.findAll('users',{token:req.body.myToken})
        .then(r1=>{
            r1=r1[0]
            if(r1===undefined){
                res.send([])
            }else{
              model.friendRequest('requests',r1.id)
              .then(r2=>{
                 res.send(r2)
               })
            }
            
          })
    }

    accept(req,res){
        let profileOwnre = req.body.myToken
        let friendId = req.body.acceptedFriendId
    
        let myself;
          model.findAll('users',{token:profileOwnre})
              .then(r1=>{
                  myself = r1[0].id
                  model.remove("requests", {user1:friendId, user2:myself})
                  
              })
              .then(r=>{
                  model.add("friendlist", {user1:friendId, user2:myself})
              })
    }

    loadFriends(req,res){
        model.findAll('users',{token:req.body.token})
        .then(r=>{
            r=r[0]
            if(r===undefined){
                res.send([])
            }else{
               model.findeFriends('users','friendlist',r.id)
                    .then(r2=>{
                      res.send(r2)
                })
            }
            
        })
    }

    decline(req,res){
        let profileOwnre = req.body.myToken
        let friendId = req.body.acceptedFriendId
        model.findAll('users',{token:profileOwnre})
             .then(r=>{
                 r=r[0].id
                 model.remove('requests',{user1:friendId,user2:r})
                      .then(res.send({success:'you have declined the friend request'}))
              })
    }

    deleteFriend(req,res){
        let profileOwnre = req.body.myToken
        let friendId = req.body.acceptedFriendId
            model.findAll('users',{token:profileOwnre})
                 .then(r=>{
                     r=r[0].id
                      model.remove('friendlist',{user1:friendId,user2:r})
                          .then(res.send({success:'you have declined the friend request'}))
                  })
    }

    usersProfile(req,res){
        model.findAll('users',{id:req.body.id})
        .then(r=>{
            r=r[0]
            res.send(r)
        })
    }

    goTo(req,res){
        model.findAll('users',{id:req.body.id})
    .then(r=>{
        r=r[0]
        res.send(r)
    })
    }

    loadPost(req,res){
        model.findAll('users',{token:req.body.token})
          .then(r=>{
              r=r[0]
              if(r===undefined){
                  res.send([])
              }else{
                model.findAll('posts',{users_id:r.id})
                .then(rr=>{
                    res.send(rr);
                })
              }
              
          })
    }

    loadNews(req,res){
        model.findAll('users',{token:req.body.token})
        .then(r=>{
            r=r[0]
            model.findAll('news',{user_id:r.id})
                 .then(rr=>{
                     res.send(rr);
                 })
        })
    }

    getAllMessages(req,res){
        model.findAll('users',{token:req.body.token})
         .then(r=>{
             r=r[0]
             model.findeFriends('users','friendlist',r.id)
                  .then(r2=>{
                  res.send(r2)
                  })
         })
    }

    blockUser(req,res){
        model.findAll('users',{id:req.body.user})
        .then(r=>{
            r=r[0]
           if(r.status===0){
               model.update('users',{id:req.body.user},{status:1})      
           }else{
               model.update('users',{id:req.body.user},{status:0})
           }
        })
    }

    setTheme(req,res){
        // return console.log(req.body);
        model.findAll('users',{token:req.body.token})
             .then(r=>{
                 r=r[0]
                 model.update('users',{id:r.id},{theme:req.body.theme})
             })
    }

    upDateTheme(req,res){
        model.findAll('users',{token:req.body.token})
             .then(r=>{
                 r=r[0]
                 res.send({data:r.theme})
             })
    }
}

module.exports = new UserController