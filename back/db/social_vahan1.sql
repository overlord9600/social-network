/*
 Navicat Premium Data Transfer

 Source Server         : VahanLesson1
 Source Server Type    : MySQL
 Source Server Version : 100316
 Source Host           : localhost:3306
 Source Schema         : social_vahan

 Target Server Type    : MySQL
 Target Server Version : 100316
 File Encoding         : 65001

 Date: 11/09/2020 04:45:36
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for chat
-- ----------------------------
DROP TABLE IF EXISTS `chat`;
CREATE TABLE `chat`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user1_id` int(11) NULL DEFAULT NULL,
  `user2_id` int(11) NULL DEFAULT NULL,
  `text` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user1_id`(`user1_id`) USING BTREE,
  INDEX `user2_id`(`user2_id`) USING BTREE,
  CONSTRAINT `chat_ibfk_1` FOREIGN KEY (`user1_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `chat_ibfk_2` FOREIGN KEY (`user2_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 38 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of chat
-- ----------------------------
INSERT INTO `chat` VALUES (35, 12, 10, 'barev', 0);
INSERT INTO `chat` VALUES (36, 12, 10, 'ara', 0);
INSERT INTO `chat` VALUES (37, 12, 10, 'ay txa', 0);

-- ----------------------------
-- Table structure for friendlist
-- ----------------------------
DROP TABLE IF EXISTS `friendlist`;
CREATE TABLE `friendlist`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user1` int(255) NULL DEFAULT NULL,
  `user2` int(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user1`(`user1`) USING BTREE,
  INDEX `user2`(`user2`) USING BTREE,
  CONSTRAINT `friendlist_ibfk_1` FOREIGN KEY (`user1`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `friendlist_ibfk_2` FOREIGN KEY (`user2`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of friendlist
-- ----------------------------
INSERT INTO `friendlist` VALUES (20, 10, 12);

-- ----------------------------
-- Table structure for news
-- ----------------------------
DROP TABLE IF EXISTS `news`;
CREATE TABLE `news`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `photo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `text` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE,
  CONSTRAINT `news_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of news
-- ----------------------------
INSERT INTO `news` VALUES (10, 12, 'asd', '9iv4brckeag3xpz15983899539111357304-free-matrix-wallpaper-for-desktop-1920x1080-picture.jpg', 'es u dzis');
INSERT INTO `news` VALUES (11, 12, 'asd', '9iv47bokeg9nkxu1598741910210favicon.jpg', 'fyhhjddfdgghj');

-- ----------------------------
-- Table structure for posts
-- ----------------------------
DROP TABLE IF EXISTS `posts`;
CREATE TABLE `posts`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `users_id` int(11) NULL DEFAULT NULL,
  `photo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `text` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `users_id`(`users_id`) USING BTREE,
  CONSTRAINT `posts_ibfk_1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of posts
-- ----------------------------
INSERT INTO `posts` VALUES (1, 12, '9iv45fkke673ydn15981329735151357304-free-matrix-wallpaper-for-desktop-1920x1080-picture.jpg', 'my world');
INSERT INTO `posts` VALUES (2, 12, '9iv45fkke676b5o1598133083388favicon.jpg', 'what is this');
INSERT INTO `posts` VALUES (3, 12, '9iv48m0ke681aps1598134529152elk.jpg', 'berry');
INSERT INTO `posts` VALUES (4, 12, '9iv4brckeafmbgg1598389131904elk.jpg', 'saddsfdsdf');

-- ----------------------------
-- Table structure for requests
-- ----------------------------
DROP TABLE IF EXISTS `requests`;
CREATE TABLE `requests`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user1` int(11) NULL DEFAULT NULL,
  `user2` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user1`(`user1`) USING BTREE,
  INDEX `user2`(`user2`) USING BTREE,
  CONSTRAINT `requests_ibfk_1` FOREIGN KEY (`user1`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `requests_ibfk_2` FOREIGN KEY (`user2`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of requests
-- ----------------------------

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `surname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `login` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `theme` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `photo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `token` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `time` datetime(0) NULL DEFAULT NULL,
  `type` int(255) NULL DEFAULT NULL,
  `block` int(255) NULL DEFAULT NULL,
  `quantity` int(255) NULL DEFAULT NULL,
  `status` int(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (9, 'Vahan', 'Muradyan', 'vahanmur007@gmail.com', '$2b$10$miAW74BFaSSQlXGxOfnIxeEiC4exwyRbD9cnYtZhA/aEr8b93wvbm', 'https://www.ejin.ru/wp-content/uploads/2019/05/kanada.jpg', '9iv49bcke4qr2xt15980450328651357304-free-matrix-wallpaper-for-desktop-1920x1080-picture.jpg', '9iv43iwkd6cf6m4', NULL, 0, NULL, NULL, 0);
INSERT INTO `users` VALUES (10, 'v', 'm', 'vahanmur@mail.ru', '$2b$10$x.rBgoNnRK0jbJKaMTP35O4ijYvnBnKWycDn9yygbpHp60LCplcKO', 'https://www.ejin.ru/wp-content/uploads/2019/05/kanada.jpg', '9iv49bcke4qr2xt15980450328651357304-free-matrix-wallpaper-for-desktop-1920x1080-picture.jpg', '9iv41n0kd6fbj6c', NULL, 0, NULL, NULL, 0);
INSERT INTO `users` VALUES (11, 'sd', 'sd', 'vahanmur0007@gmail.com', '$2b$10$jb.fUxJZtvGYiPH0Ze8i9OOD9omq48pwJoD/PfG2AftT5ebog.DiO', 'https://www.ejin.ru/wp-content/uploads/2019/05/kanada.jpg', '9iv49bcke4qr2xt15980450328651357304-free-matrix-wallpaper-for-desktop-1920x1080-picture.jpg', '9iv495kkd80pape', '0000-00-00 00:00:00', 0, NULL, NULL, 0);
INSERT INTO `users` VALUES (12, 'asd', 'asd', 'vahanmur111@mail.ru', '$2b$10$ry8fKO3/5VFIBdfhAaChmuPwIqAwY4aAGL6pKb5/LuvnTb44TNTai', 'https://www.ejin.ru/wp-content/uploads/2019/05/kanada.jpg', '9iv4brckeafonmq1598389240994favicon.jpg', '9iv424wkexioi3b', '0000-00-00 00:00:00', 0, 0, 0, 0);
INSERT INTO `users` VALUES (13, 'Harut', 'Ghukasyan', 'har.gh@mail.ru', '$2b$10$Nqk0jmuZnHGoXRp/eyA2A.4PlXNNqd21mUzyVApBVQwFvTsKPZCRO', NULL, '9iv49bcke4qr2xt15980450328651357304-free-matrix-wallpaper-for-desktop-1920x1080-picture.jpg', '9iv496ske7l1rfu', NULL, 0, NULL, NULL, 0);
INSERT INTO `users` VALUES (15, 'Harut', 'Ghukasyan', 'hargh@mail.ru', '$2b$10$k26p3.ty6fGr3VWoTXglkOwh9bWTg0xam9g7WjN/pWqd5iM78ySTa', NULL, '9iv496ske7l3jv61598216935506elk.jpg', '9iv496ske7l4zpo', '0000-00-00 00:00:00', 0, NULL, NULL, 0);

SET FOREIGN_KEY_CHECKS = 1;
