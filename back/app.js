const express = require("express");
const app = express();
 
const Socket = require('./lib/socket')


const http = require("http");
const socketIo = require("socket.io");

const port = 5000;

const server = http.createServer(app);

const io = socketIo(server);


io.on("connection", (socket) => {
    let connection=new Socket(socket)
    
    
  });



const model = require('./lib/model')
var bodyParser = require('body-parser')
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(express.static('images'))
const multer = require('multer')
const bcrypt = require('bcrypt');
const saltRounds = 10;
const uniqid=require("uniqid");
const UserController = require('./controller/userController')
app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

var storage = multer.diskStorage({
    destination: (req,file,cb)=>{
        cb(null, 'images')
    },
    filename:(req,file,cb)=>{
        cb(null,uniqid()+Date.now()+file.originalname)
    }
})

var upload = multer({storage:storage})

app.get("/",(req,res)=>{
    res.send("OK")
})

//registration
app.post("/add",(req,res)=>UserController.add(req,res))

//login
app.post('/login',(req,res)=>UserController.login(req,res))

//get all users for admin
app.post('/foradmin',(req,res)=>UserController.usersForAdmin(req,res)) 

//load profile
app.post('/profile',(req,res)=>UserController.loadProfile(req,res))

//change login
app.post('/resetlogin',(req,res)=>UserController.changeLogin(req,res))

//change password
app.post('/resetpassword',(req,res)=>UserController.changePwd(req,res))

//search 
app.post('/friendsearch',(req,res)=>UserController.searchSis(req,res))

//friend request
app.post('/request',(req,res)=>UserController.request(req,res))

//load all requests
app.post('/loadrequests',(req,res)=>UserController.loadRequests(req,res))

//accept friend from requests
app.post('/acceptfriend',(req,res)=>UserController.accept(req,res)) 

//load friends in friend list
app.post('/loadfriends',(req,res)=>UserController.loadFriends(req,res))

//decline friends from requests
app.post('/declinefriend',(req,res)=>UserController.decline(req,res))

// delete friend from friend list
app.post('/delfriend',(req,res)=>UserController.deleteFriend(req,res))

//go to users profile
app.post('/gotoprofile',(req,res)=>UserController.usersProfile(req,res))

//friend's profile
app.post('/updateFriendProfile',(req,res)=>UserController.goTo(req,res))

//upload photo
app.post('/upload',upload.single('nkar'),(req,res)=>{
    let token = req.body.user
    
    model.findAll('users',{token})
         .then(r=>{
             if(r.length>0){
                 r=r[0]
                  model.update('users',{id:r.id},{photo:req.file.filename})
                      .then(rr=>{
                          res.send('/profile')
                      })
             }
         })
})

app.post('/addpost',upload.single('postnkar'),(req,res)=>{
    let token = req.body.user
    let post = req.body.post
     model.findAll('users',{token})
         .then(r=>{
             r=r[0]
             if(req.file===undefined&&post.length!==0){
                model.add('posts', {users_id:r.id,text:post})
             }else if(post.length==0&&req.file!==undefined){
                model.add('posts', {users_id:r.id,photo:req.file.filename})
            }else if(req.file===undefined&&post.length==0){
                 res.send({error:'Fill in something'})
             }else{
                model.add('posts', {users_id:r.id,text:post,photo:req.file.filename})
            }
         })
 })

 app.post('/loadposts',(req,res)=>UserController.loadPost(req,res))

 app.post('/setnews',upload.single('publicnkar'),(req,res)=>{
    model.findAll('users',{token:req.body.user})
         .then(r=>{
             r=r[0]
             if(req.file===undefined&&req.body.text.length!==0){
                model.add('news',{user_id:r.id,text:req.body.text,name:r.name})
             }else if(req.body.text.length==0&&req.file!==undefined){
                model.add('news',{user_id:r.id,photo:req.file.filename,name:r.name})
             }else if(req.file===undefined&&req.body.text.length==0){
                 res.send({error:'Fill in something'})
             }else{
                model.add('news',{user_id:r.id,name:r.name,text:req.body.text,photo:req.file.filename})
             }
              
         })
 })

 app.post('/loadnews',(req,res)=>UserController.loadNews(req,res))
 
  //finde messages
 app.post('/friendmessage',(req,res)=>UserController.getAllMessages(req,res))
 
//admins action
 app.post('/blockUser',(req,res)=>UserController.blockUser(req,res))

 app.post('/settheme',(req,res)=>UserController.setTheme(req,res))

 app.post('/updatetheme',(req,res)=>UserController.upDateTheme(req,res))
 
server.listen(5000)
console.log("server started")