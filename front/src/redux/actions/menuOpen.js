export function handleDrawerToggle(){
    return{
        type:'handleDrawerToggle'
    }
}

export function handleExpandClick(){
    return {
        type:'handleExpandClick'
    }
}