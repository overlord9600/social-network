import axios from 'axios'

export function searchInput(key, value){
    return (dispatch)=>{
        dispatch(searchInputChange(key, value))
         
            axios.post('http://localhost:5000/friendsearch',{text:value})
                 .then(r=>{
                     console.log(r.data);
                     if(r.data){
                        dispatch(updateSearchedUser(r.data))
                     }else{
                        dispatch(updateSearchedUser([]))
                     }
                     
                 })
        

    }
}

export function updateSearchedUser(data){
    return{
        type:'updateSearchedUser',data
    }
}

export function searchInputChange(key, value){
    return{
        type:'searchInputChange',key, value
    }
}


export function addToFriendsReq(clickedFriend){
    return (dispatch)=>{
        axios.post('http://localhost:5000/request', {id:clickedFriend,myToken:sessionStorage.currentUser})
    }
}

export function loadRequests(){
    return (dispatch)=>{
        axios.post('http://localhost:5000/loadrequests',{myToken:sessionStorage.currentUser})
        .then(r=>{
             dispatch(showRequests(r.data))
        })
    }
}

export function showRequests(data){
    return {type:'showRequests',data}
}

export function addToFriendList(acceptedFriend,index,requests){
    return (dispatch)=>{
        axios.post('http://localhost:5000/acceptfriend',{myToken:sessionStorage.currentUser, acceptedFriendId:acceptedFriend})
        requests.splice(index,1)
    }
}



export function deletaFromReq(declinedFriend,requests){
    return (dispatch)=>{
        axios.post('http://localhost:5000/delfriend',{myToken:sessionStorage.currentUser,acceptedFriendId:declinedFriend})
        requests.splice(declinedFriend,1)
    }
}
 
export function updateFriends(){
    return (dispatch)=>{
        axios.post('http://localhost:5000/loadfriends',{token:sessionStorage.currentUser})
              .then(r=>{
                  dispatch(updateFriendsList(r.data))
              })
    }
}

export function updateFriendsList(data){
    return {
        type:'updateFriendsList',
        data
    }
}

export function deletaFromFriends(id,index){
    return {
        type:'deletaFromFriends',
        index:index,
        id:id
    }
}

export function goToProfile(id,history){
    return {
        type:'goToProfile',
        profile:id,
        history:history
    }
}

export function updateFriendProfile(friendProfile){
    return {
        type:'updateFriendProfile',
        key:friendProfile
    }
}