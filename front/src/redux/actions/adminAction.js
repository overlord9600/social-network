import axios from 'axios'

export function loadAllUsers(){
    return (dispatch)=>{
        axios.post('http://localhost:5000/foradmin')
        .then(r=>{
           dispatch(updateUsers(r.data))
        })
    }
}

export function updateUsers(key){
    return {
        type:'updateUsers',
        key
    }
}

export function blockUser(user,e){
    return (dispatch)=>{
        axios.post('http://localhost:5000/blockUser',{user:user})
    if(e.className=='unblocked'){
     e.className='blocked'
    }else{
     e.className='unblocked'
    }
    }
}
