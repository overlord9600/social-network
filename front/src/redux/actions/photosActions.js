import axios from 'axios'

export function addPost(postText){
    return (dispatch)=>{
        let form =new FormData(document.getElementById('myPost'))
        form.append('user', sessionStorage.currentUser)
        form.append('post',postText)
         axios.post('http://localhost:5000/addpost',form)
    }
}

export function changePostText(key,value){
    return {
        type:'ChangePostText',
        key,
        value
    }
}

export function loadPosts(){
    return (dispatch)=>{
        axios.post('http://localhost:5000/loadposts',{token:sessionStorage.currentUser})
             .then(r=>{
                 dispatch(loadPhotos(r.data))
             })
    }
}

export function loadPhotos(key){
    return{
        type:'loadPhotos',
        key
    }
}

export function photoChange(item){
    return {
        type:'photoChange',
        item
    }
}

export function outBlock(item){
    return {
        type:'outBlock',
        item
    }
}
