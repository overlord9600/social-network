import axios from 'axios'

export function changeInput(key,value){
    return{
        type:'changeInput',
        key,
        value
    }
}

export function changeInput1(key,value){
    return{
        type:'changeInput1',
        key,
        value
    }
}

export function userValidate(props){
   return (dispatch)=>{
    axios.post('http://localhost:5000/add',props.user.signup) 
    .then(r=>{
        console.log(r.data);
        if('success' in r.data){
            props.history.push('/')
            dispatch(setError1(r.data.success))
        }else{
            dispatch(setError1(r.data.error))
        }
     })
   }
}

export function setError1(key){
    return {
        type:'setError1',
        key
    }
}

export function userLogin(props){
    return function(dispatch){
        // temp.errorsin.error='ok'
                axios.post('http://localhost:5000/login',props.user.signin)
                     .then(r=>{
                           if("success" in r.data){
                            props.user.errorsup.error=''
                              sessionStorage.currentUser = r.data.token
                              props.history.push("/profile");
                              console.log(r.data);
                           }else if('adminsuccess' in r.data){
                              sessionStorage.currentUser = r.data.token
                              props.history.push("/admin/dashboard");
                            //   console.log(r.data);
                           }else{
                            props.user.errorsup.error=''
                              dispatch(setError2(r.data.error))
                            //   console.log(r.data);
                           }
                     })
             } 
}

export function setError2(key){
    return {
        type:'setError2',
        key
    }
}

export function loadProfile(key,props){ 
    return (dispatch)=>{
        axios.post('http://localhost:5000/profile', { token: key })
       .then(r => {
            if ("success" in r.data){
               sessionStorage.currentUser = r.data.token;
               dispatch(updateProfile(r.data.user))
            } else if ("error" in r.data) {
               delete  sessionStorage.currentUser
               dispatch(setError3(r.data.error));
               props.history.push("/");
           }
       })
    }
}

export function updateProfile(user){
    return{
        type:'updateProfile',
        user
    }
}

export function setError3(key){
    return{
        type:'setError3',
        key
    }
}

export function LogOut(){
    return{
        type:'logout'
    }
}
export function ChangeLogin(){
    return{
        type:'ChangeLogin',
    }
}
export function changeInputInLoginChanges(key,value){
    return{
        type:'changelogin',
        key,
        value
    }
}
export function checkLogin(props){
    return (dispatch)=>{
        axios.post('http://localhost:5000/resetlogin',{login:props.newLogin, password:props.password, token:sessionStorage.currentUser})
                 .then(r=>{
                    if('error' in r.data){
                       dispatch(setError4(r.data.error))
                    }else{
                       dispatch(setError4(r.data.success))
                    }
                 })
    }
}

export function setError4(key){
    return{
        type:'setError4',
        key
    }
}

export function changeInputInPasswordChanges(key,value){
    return {
        type:'changePasswords',
        key,
        value
    }
}

export function checkPasswords(password){
    return (dispatch)=>{
        axios.post('http://localhost:5000/resetpassword',{newpassword:password.newPassword, oldpassword:password.oldPassword, token:sessionStorage.currentUser})
              .then(r=>{
                 if('error' in r.data){
                     dispatch(setError5(r.data.error))
                 }else{
                    dispatch(setError5(r.data.success))
                 }
              })
    }
}

export function setError5(key){
    return{
        type:'setError5',
        key
    }
}

export function upload(history){
    return (dispatch)=>{
        let form = new FormData(document.getElementById('myForm'))
        form.append("user", sessionStorage.currentUser)
        axios.post('http://localhost:5000/upload',form)
             .then(r=>{
                history.push(r.data)
             })
    }
}

export function changeTheme(photo){
    return (dispatch)=>{
        axios.post('http://localhost:5000/settheme',{theme:photo,token:sessionStorage.currentUser})
        dispatch(updateTheme(photo))

    }
}

export function loadTheme(){
    return (dispatch)=>{
        axios.post('http://localhost:5000/updatetheme',{token:sessionStorage.currentUser})
             .then(r=>{
                dispatch(updateTheme(r.data))
             })
    }
}

export function updateTheme(key){
    return {
        type:'updateTheme',
        key
    }
}

