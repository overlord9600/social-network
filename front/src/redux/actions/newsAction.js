import axios from 'axios'

export function addNews(text){
    return (dispatch)=>{
        let forms =new FormData(document.getElementById('myNews'))
        forms.append('user',sessionStorage.currentUser)
        forms.append('text',text)
         
        axios.post('http://localhost:5000/setnews',forms)
    }
}

export function newsTextChange(key,value){
    return {
        type:'newsTextChange',
        key,
        value
    }
}


export function loadNews(){
    return (dispatch)=>{
        axios.post('http://localhost:5000/loadnews',{token:sessionStorage.currentUser})
        .then(r=>{
               dispatch(updateNews(r.data))
        })
    }
}

export function updateNews(data){
    return {
        type:'updateNews',
        data
    }
}