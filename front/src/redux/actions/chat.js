import axios from 'axios'

export function freandsChatList(){
    return (dispatch)=>{
        axios.post("http://localhost:5000/friendmessage", {token:sessionStorage.currentUser})
        .then(r=>{
            dispatch(updateChatFreands(r.data))
        })
    }
}

export function updateChatFreands(key){
    return {
        type:'updateChatFreands',
        key
    }
}

export function startChat(id, socket){
    return {
        type:"startChat",
        id,
        socket
    }
}

export function changeInputOfMessage(value){
    return {
        type:"changeInputOfMessage",
        value
    }
}

export function sendMessage(key,socket){
    return {
        type:"sendMessage",
        key,
        socket
    }
}

export function loadMessages(){
    return {
        type:'loadMessages'
    }
}

export function showMessages(data){
     return {
        type:'showMessages',
        data
    }
}