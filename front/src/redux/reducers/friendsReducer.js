import friendsState from '../state/friends'
import axios from 'axios'

 function friendsReducer(state = friendsState, action){
    const temp = {...state}

    if(action.type==='searchInputChange'){
        temp.searchText = action.value
         return temp
     }
      if(action.type==='updateSearchedUser'){
         temp.searchedUsers=action.data
          return temp
     }

     if(action.type==='showRequests'){
         temp.requests=action.data
         return temp
     }

      if(action.type==='updateFriendsList'){
         temp.friendList=action.data
          return temp
     }

     if(action.type==='deletaFromReq'){
         temp.requests.splice(action.index,1)
         return temp
     }
     if(action.type==='deletaFromFriends'){
         axios.post('http://localhost:5000/delfriend',{myToken:sessionStorage.currentUser,acceptedFriendId:action.id})
         temp.friendList.splice(action.index,1)
     }

    //  if(action.type==='goToProfile'){
         
    //      axios.post('http://localhost:5000/gotoprofile',{id:action.profile})
    //           .then(r=>{
    //               temp.friendProfile=r.data
    //             action.history.push('/goto')
                
    //            })
    //           return temp
    //   }else if(action.type==='updateFriendProfile'){
    //      temp.friendProfile=action.data
    //      return temp
    //  }



     return temp;
}

export default friendsReducer