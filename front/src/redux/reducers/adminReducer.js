import adminState from '../state/adminState'
import axios from 'axios'

export default function adminReducer(state=adminState,action){
    const temp = {...state}

     if(action.type==='updateUsers'){
        temp.usersforAdmin=action.key
        return temp
     }

    return temp
}
