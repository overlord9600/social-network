import mobileOpenState from '../state/menuOpenState'

export default function menuOpen(state=mobileOpenState,action){
    let temp = {...state}

        if(action.type==='handleDrawerToggle'){
             if(temp.mobileOpen==false){
                temp.mobileOpen=true
             }else{
                temp.mobileOpen=false 
             }
            return temp
        }

        if(action.type==='handleExpandClick'){
             if(temp.expanded==false){
                temp.expanded=true
             }
            return temp
            
        }


        return temp
}