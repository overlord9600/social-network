import photoState from '../state/photoState'

function photosReducer(state=photoState,action){
    let temp = {...state} 

    if(action.type==='ChangePostText'){
        temp[action.key]=action.value
        return temp
    }

    if(action.type==='loadPhotos'){
        temp.photo=action.key
        return temp
    }
     
    if(action.type==='photoChange'){
        document.body.style.overflow='hidden'

        temp.slide=action.item
        temp.slidePhotos=temp.photo.map(item=>item.photo)
         return temp
    }
    if(action.type==='outBlock'){
        document.body.style.overflow='unset'
            temp.slide=''
            return temp
    }
   

    return temp
}

export default photosReducer