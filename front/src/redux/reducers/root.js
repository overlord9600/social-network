import {combineReducers} from 'redux'
import user from './userReducer'
import friends from './friendsReducer'
import chat from './messageReducer'
import photo from './photosReducer'
import news from './newsReducer'
import menu from './menuOpenReducer'
import admin from './adminReducer'
let root = combineReducers({
    user:user,
    friends:friends, 
    chat:chat,
    photos:photo,
    news:news,
    menuOpen:menu,
    admin:admin
})
export default root;