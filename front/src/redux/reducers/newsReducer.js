import newsState from '../state/newsState'

function newsReducer(state=newsState,action){
    let temp = {...state}
        if(action.type==='newsTextChange'){
            temp[action.key]=action.value
            return temp
        }

        if(action.type==='updateNews'){
            temp.loadedNews=action.data
            return temp
        }


    return temp
}

export default newsReducer