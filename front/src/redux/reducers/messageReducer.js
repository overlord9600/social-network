import chatState from '../state/chat'
import axios from 'axios'

function chatReducer(state = chatState, action){

    let temp = {...state}
   
    if(action.type == "updateChatFreands"){
        temp.frends = action.key
        return temp;
    }
    
    if(action.type =="startChat"){
        temp.currentUser = action.id;
          action.socket.emit("startChat",{id:action.id,me: sessionStorage.currentUser})
       
        return temp;
    }

    if(action.type =="changeInputOfMessage"){
        temp.text = action.value;
        return temp;
    }
    if(action.type == "sendMessage"){
        if(action.key == "Enter"){
              action.socket.emit('newMessage',{text:temp.text,token:sessionStorage.currentUser,friend:temp.currentUser}) 
            temp.text="";
            return temp;
        }
    }

   

    if(action.type==='updateMessages'){
        temp.messenger=action.data
        return temp
    }

    if(action.type==='showMessages'){
        temp.messenger=action.data
        return temp
    }

    return temp;
}

export default chatReducer