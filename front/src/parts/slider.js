import React, { useState, useEffect } from 'react'
import '../App.css'
import { connect } from 'react-redux'
import { outBlock } from '../redux/actions/photosActions'
import NavigateBeforeIcon from '@material-ui/icons/NavigateBefore';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
 

import { makeStyles, useTheme } from '@material-ui/core/styles';
import MobileStepper from '@material-ui/core/MobileStepper';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import SwipeableViews from 'react-swipeable-views';
import { autoPlay } from 'react-swipeable-views-utils';

const AutoPlaySwipeableViews = autoPlay(SwipeableViews);

const useStyles = makeStyles((theme) => ({
    root: {
    //   maxWidth: 400,
      flexGrow: 1,
    },
    header: {
      display: 'flex',
      alignItems: 'center',
      height: 50,
      paddingLeft: theme.spacing(4),
      backgroundColor: theme.palette.background.default,
    },
    img: {
      height: 255,
      display: 'block',
    //   maxWidth: 400,
      overflow: 'hidden',
      width: '100%',
    },
  }));

 function Slider(props){
    const classes = useStyles();
    const theme = useTheme();
    const [activeStep, setActiveStep] = useState(0);
    const maxSteps = props.photos.photo.length;
  
    const handleNext = () => {
      setActiveStep((prevActiveStep) => prevActiveStep + 1);
    };
  
    const handleBack = () => {
      setActiveStep((prevActiveStep) => prevActiveStep - 1);
    };
  
    const handleStepChange = (step) => {
      setActiveStep(step);
    };
      
     return(
        <div className={!props.photos.slide?'notBgMask':'bgMask'}>
            <div className='closeBtn' onClick={(e)=>props.dispatch(outBlock(e.target))}>
            <HighlightOffIcon style={{ color: "white",float:'right',margin:50,fontSize:90,cursor:'pointer' }}/>
            </div>
            
            <div className='images'>
                <div className={classes.root}>
                  <Paper square elevation={0} className={classes.header}>
                    <Typography>{!props.photos.photo[activeStep]?null:props.photos.photo[activeStep].text}</Typography>
                  </Paper>
                  <AutoPlaySwipeableViews
                    axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
                    index={activeStep}
                    onChangeIndex={handleStepChange}
                    enableMouseEvents
                  >
                    {props.photos.photo.map((step, index) => (
                      <div key={step.text}>
                        {Math.abs(activeStep - index) <= 2 ? (
                          <img className={classes.img} src={`http://localhost:5000/${step.photo}`} alt={step.text} />
                        ) : null}
                      </div>
                    ))}
                  </AutoPlaySwipeableViews>
                  <MobileStepper
                    steps={maxSteps}
                    position="static"
                    variant="dots"
                    activeStep={activeStep}
                    nextButton={
                      <Button size="small" onClick={handleNext} disabled={activeStep === maxSteps - 1}>
                        Next
                        {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
                      </Button>
                    }
                    backButton={
                      <Button size="small" onClick={handleBack} disabled={activeStep === 0}>
                        {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
                        Back
                      </Button>
                    }
                  />
                </div>
            </div>
        </div>
    )
}

export default connect(r=>r)(Slider)