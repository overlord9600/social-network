import React from 'react'
import { connect } from 'react-redux'
import {handleDrawerToggle} from '../redux/actions/menuOpen'
import Bg from '../components/bgPhoto.js/bg';
import LoggedInNavigation from './LoggedInNavigation';
import Drawer from '@material-ui/core/Drawer';
import Hidden from '@material-ui/core/Hidden';
import { useTheme } from '@material-ui/core/styles';
import '../components/profileComponents/profile.css'
import { useStyles } from '../components/profileComponents/profilestyles';

function Nav(props){
    const { window } = props;
  
    const container = window !== undefined ? () => window().document.body : undefined;
    const classes = useStyles();
    const theme = useTheme();
    return(
        <nav className={classes.drawer} aria-label="mailbox folders">
          {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
          <Hidden smUp implementation="css">
            <Drawer
              container={container}
              variant="temporary"
              anchor={theme.direction === 'rtl' ? 'right' : 'left'}
              open={props.menuOpen.mobileOpen}
              onClose={()=>props.dispatch(handleDrawerToggle())}
              classes={{
                paper: classes.drawerPaper,
              }}
              ModalProps={{
                keepMounted: true, // Better open performance on mobile.
              }}
            >
              <LoggedInNavigation/>
            </Drawer>
            <Bg/>
          </Hidden>
          <Hidden xsDown implementation="css">
            <Drawer
              classes={{
                paper: classes.drawerPaper,
              }}
              variant="permanent"
              open
            >
               <LoggedInNavigation/>
            </Drawer>
              <Bg/>
          </Hidden>
        </nav>
    )
}

export default connect(r=>r)(Nav)