import React from 'react'
import { searchInputChange } from '../redux/actions/friends'
import { LogOut } from '../redux/actions/user'
import { connect } from 'react-redux'
import {handleDrawerToggle} from '../redux/actions/menuOpen'
import { Link } from 'react-router-dom';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import logo from '../logo.png'
import InputBase from '@material-ui/core/InputBase';
import SearchIcon from '@material-ui/icons/Search';
import '../components/profileComponents/profile.css'
import { useStyles } from '../components/profileComponents/profilestyles';
import { AppBar } from '@material-ui/core'

 function ToolBar(props){
    const classes = useStyles();
console.log('itm',props.friends.searchedUsers);
    const searchResults=props.friends.searchedUsers.map((item)=>{
        return <li key={item.id}><Link to='/profile/search'>{item.name} {item.surname}</Link></li>
        })
    return(
      <AppBar position="fixed" style={{backgroundColor:'#212529'}} className={classes.appBar}>
        <Toolbar>
                 
        <IconButton
          id='burgerbtn'
          color="inherit"
          aria-label="open drawer"
          edge="start"
          onClick={()=>props.dispatch(handleDrawerToggle())}
          className={classes.menuButton}
        >
          <MenuIcon />
        </IconButton>
        
        <img src={logo} width='40' height='50'/>
        <Typography variant="h6" noWrap>
          Shield Network
        </Typography>
        <div style={{display:'flex'}}>
        <div className={classes.search}>
          <div id="autocomplete" className="autocomplete">
          <div className={classes.searchIcon}>
            <SearchIcon />
          </div>
          <InputBase
            id='searchInp'
            placeholder="Search…"
            value={props.friends.searchText}
            onChange={(e)=>props.dispatch(searchInputChange('searchedUsers',e.target.value))}
            classes={{
              root: classes.inputRoot,
              input: classes.inputInput,
            }}
            value={props.friends.searchText}
            inputProps={{ 'aria-label': 'search' }}
          />
        </div>
          <ul className="autocomplete-result-list">
            {
              props.friends.searchText.length>0 && props.friends.searchedUsers.length===0?(
                <li className="autocomplete-result">
                  No results found
                </li>
              ):props.friends.searchedUsers.length>4?(
                <li className="autocomplete-result">
                  <Link to='/profile/search'>See all results</Link>
                </li>
              ):searchResults
            }
          </ul>
          
           
        </div>
        <Link style={{color:'white'}} to="/"
         onClick={()=>props.dispatch(LogOut())}
         >Log out</Link>
        </div>
       </Toolbar>
     </AppBar>
    )
}

export default connect(r=>r)(ToolBar)