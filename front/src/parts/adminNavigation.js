import React, { useState, useEffect } from 'react';
import PersonIcon from '@material-ui/icons/Person'; 
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Typography from '@material-ui/core/Typography';
import { Link } from 'react-router-dom';
import '../components/profileComponents/profile.css'
import { useStyles } from '../components/profileComponents/profilestyles';
import { connect } from 'react-redux';
import { updateFriends, loadRequests } from '../redux/actions/friends';
import { loadPosts } from '../redux/actions/photosActions';
 
function AdminNavigation(props){
  const classes = useStyles();
  let photo = 'http://localhost:5000/'+props.user.profile.photo
  let profile=props.user.profile
  const [load, setLoad] = useState(true)
  useEffect(()=>{
     if(load == true){
         setLoad(false)
      }
 })
  return(
    <div>
      <div className={classes.toolbar} style={{padding:15}}>
        <img src={photo} width='100' height='100' style={{borderRadius:50}}/>
        <Typography color='primary'>{profile.name} {profile.surname}</Typography>
      </div>
       <List>
      <Link to='/admin/dashboard' className={classes.links}>
      <ListItem button>
      <ListItemIcon>
        <PersonIcon/><ListItemText>Profile</ListItemText>
        </ListItemIcon>
       </ListItem>
       </Link>
       </List>
    </div>
  )
}

export default connect(r=>r)(AdminNavigation)