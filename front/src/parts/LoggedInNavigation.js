import React, { useState, useEffect } from 'react';
import PersonIcon from '@material-ui/icons/Person';
import SupervisorAccountIcon from '@material-ui/icons/SupervisorAccount';
import SettingsIcon from '@material-ui/icons/Settings';
import PhotoIcon from '@material-ui/icons/Photo';
import Badge from '@material-ui/core/Badge';
import NotificationsIcon from '@material-ui/icons/Notifications';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import MailIcon from '@material-ui/icons/Mail';
import Typography from '@material-ui/core/Typography';
import { Link } from 'react-router-dom';
import '../components/profileComponents/profile.css'
import { useStyles } from '../components/profileComponents/profilestyles';
import { connect } from 'react-redux';
import { updateFriends, loadRequests, showRequests } from '../redux/actions/friends';
import { loadPosts } from '../redux/actions/photosActions';
import { changeTheme } from '../redux/actions/user';
 
function Navigation(props){
  const classes = useStyles();
  let photo = 'http://localhost:5000/'+props.user.profile.photo
  let profile=props.user.profile
  const [load, setLoad] = useState(true)
  const [themeAction,setAction] = useState(false)

      useEffect(()=>{
                props.dispatch(updateFriends())
                props.dispatch(loadPosts())
                // props.dispatch(loadRequests())
      },[])
  
  return(
    <div>
      <div className={classes.toolbar} style={{padding:15}}>
        <img src={photo} width='100' height='100' style={{borderRadius:50}}/>
        <Typography color='primary'>{profile.name} {profile.surname}</Typography>
      </div>
       <List>
      <Link to='/profile' className={classes.links}>
      <ListItem button>
      <ListItemIcon>
        <PersonIcon/><ListItemText>Profile</ListItemText>
        </ListItemIcon>
       </ListItem>
       </Link>
      <Link to='/profile/friends' className={classes.links}>
      <ListItem button>
      <ListItemIcon>
      <Badge badgeContent={props.friends.friendList.length} color="secondary">
      <SupervisorAccountIcon/>
              </Badge>
        
        <ListItemText>Friends</ListItemText>
        </ListItemIcon>
       </ListItem>
       </Link>
       <Link to='/profile/photos' className={classes.links}>
      <ListItem button><ListItemIcon>
      <Badge badgeContent={props.photos.photo.length} color="secondary">
      <PhotoIcon/></Badge>
        <ListItemText>Photos</ListItemText>
        </ListItemIcon>
       </ListItem>
       </Link>
       <Link to='/profile/messanger' className={classes.links}>
      <ListItem button>
      <ListItemIcon>
      <Badge badgeContent={0} color="secondary">
                <MailIcon />
              </Badge>
         <ListItemText>Messages</ListItemText>
        </ListItemIcon>
       </ListItem>
       </Link>
       <Link to='/profile/requests' className={classes.links}>
      <ListItem button>
      <ListItemIcon>
      <Badge 
       badgeContent={props.friends.requests.length}
       color="secondary">
                <NotificationsIcon />
              </Badge>
         <ListItemText>Requests</ListItemText>
        </ListItemIcon>
       </ListItem>
       </Link>
       
      </List>
      
       <List>
       <Link to='/profile/settings' className={classes.links}>
      <ListItem button>
      <ListItemIcon>
        <SettingsIcon/><ListItemText>Settings</ListItemText>
        </ListItemIcon>
       </ListItem>
       </Link>
      </List>
      <ul className='theme'>
        <li onClick={()=> themeAction==false?setAction(true):setAction(false)}>Change Theme</li>
        <div className={themeAction==false?'showThemes':'hideThemes'}>
          {
            props.photos.photo.map((item,index)=>{
              return <img 
                      src={`http://localhost:5000/${item.photo}`} 
                      width='50' height='50' 
                      key={item.id}
                      onClick={()=>props.dispatch(changeTheme(item.photo))}
                      />
            })
          }
        </div>
      </ul>
       
    </div>
  )
}

export default connect(r=>r)(Navigation)