import React from 'react'
import { Switch, Route, BrowserRouter } from 'react-router-dom'
 import Profile from './components/profileComponents/profileComponents'

import { connect } from 'react-redux'
import signComponent from './components/signcomponents/signComponent'
import signupComponent from './components/signcomponents/signupComponent'
import settingsComponent from './components/settingsComponent'
import searchComponent from './components/searchComponent'
import friendsComponent from './components/friendsComponent'
import requestsComponent from './components/requestsComponent'
import photosComponent from './components/photosComponent'
import Messanger from './components/messanger/messanger'
import Admin from './components/adminComponent/admin'

function App(){
  return(
    <BrowserRouter>
      <Route exact path='/' component={signComponent}/>
      <Route exact path='/signup' component={signupComponent}/>
      <Route exact path='/profile/settings' component={settingsComponent}/>
      <Route exact path='/profile/search' component={searchComponent}/>
      <Route exact path='/profile/friends' component={friendsComponent}/>
      <Route exact path='/profile/requests' component={requestsComponent}/>
      <Route exact path='/profile/photos' component={photosComponent}/>
      <Route exact path='/profile/messanger' component={Messanger}/>
      <Route exact path='/profile' component={Profile}/>
      <Route exact path='/admin/dashboard' component={Admin}/>
    </BrowserRouter>
   )
}

export default connect(r=>r)(App)