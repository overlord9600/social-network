import React, { useState, useEffect } from 'react';
import '../../App.css'
import { connect } from 'react-redux';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import { loadProfile } from '../../redux/actions/user';
import AddPhotoAlternateIcon from '@material-ui/icons/AddPhotoAlternate';
import './profile.css'
import { useStyles } from './profilestyles';
import { Paper, Grid, Divider, TextField, Button, TextareaAutosize,Typography, IconButton } from '@material-ui/core';
import { newsTextChange, addNews, loadNews } from '../../redux/actions/newsAction';
import clsx from 'clsx';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import Avatar from '@material-ui/core/Avatar';
import FavoriteIcon from '@material-ui/icons/Favorite';
import ShareIcon from '@material-ui/icons/Share';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ToolBar from '../../parts/ToolBar';
import NavBar from '../../parts/NavBar';
import socketIOClient from "socket.io-client";
import { loadRequests, updateFriends } from '../../redux/actions/friends';
import { loadPhotos, loadPosts } from '../../redux/actions/photosActions';

 const ENDPOINT = "http://127.0.0.1:5000";
 const socket = socketIOClient(ENDPOINT);


function ProfileComponent(props) {
  console.log(props);
          useEffect(()=>{
            let key = sessionStorage.currentUser

            async function fetchData(){
             await props.dispatch(loadProfile(key,props.history))
              props.dispatch(loadNews())
              props.dispatch(loadPosts())
            }
            fetchData()
                // props.dispatch( updateFriends())
                // props.dispatch(loadPosts())
                // props.dispatch(loadRequests())
          },[])
    const classes = useStyles();     
    const [expanded, setExpanded] = useState(false);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };
   
  console.log(props.friends.friendList);
    let photo = 'http://localhost:5000/'+props.user.profile.photo
     return (
        <div className={classes.root}>
        <CssBaseline />
           <ToolBar/>
         <NavBar/>
        <main className={classes.content}>
          <div className={classes.toolbar}></div>
           <div className={classes.root3}>
      <Grid container spacing={3}>
         <Grid item xs={8}>
          <Paper className={classes.paper2} elevation={4}>
          <div className='publishings'>
              <div style={{marginRight:30}}>
                  <img src={photo} height='100' width='100'/>
               </div>
              <div className='publicMaterials'>
                  <TextareaAutosize 
                  cols='60' 
                  id='newsText'
                  placeholder='Write something'
                  value={props.news.newsText}
                  onChange={(e)=>props.dispatch(newsTextChange('newsText',e.target.value))}
                  />
                  <form style={{margin:'auto'}} method='post' id='myNews' encType='multipart/form-data'>
                    <label htmlFor='publicnkar'>
                      <input
                        style={{ display: "none" }}
                        id="publicnkar"
                        name="publicnkar"
                        type="file"
                      />
                      <AddPhotoAlternateIcon/>
                    </label>
                  </form>
                  <Button 
                  size='small' 
                  color='secondary' 
                  variant="contained"
                  onClick={()=>props.dispatch(addNews(props.news.newsText))}
                  >Publsh</Button>
               </div>
             </div>
            
            <Divider/>
            <h3>News</h3>
              <Divider/>
              {
                props.news.loadedNews.map(item=>{
                  return(
                    <Card className={classes.root4} key={item.id}>
                      <CardHeader
                        avatar={
                          <Avatar aria-label="recipe" className={classes.avatar}>
                            <img src={photo}/>
                          </Avatar>
                        }
                        //post title
                        title={item.name}
                        //post date
                        subheader="September 14, 2016"
                      />
                      <CardMedia
                        style={!item.photo?{display:'none'}:{display:'block'}}
                        className={classes.media}
                        image={`http://localhost:5000/${item.photo}`}
                      />
                      <CardContent>
                        <Typography variant="body2" color="textSecondary" component="p">
                          {item.text}
                        </Typography>
                      </CardContent>
                      <CardActions disableSpacing>
                        <IconButton aria-label="add to favorites">
                          <FavoriteIcon />
                        </IconButton>
                        <IconButton aria-label="share">
                          <ShareIcon />
                        </IconButton>
                        <IconButton
                          className={clsx(classes.expand, {
                            [classes.expandOpen]: expanded,
                          })}
                          onClick={handleExpandClick}
                          aria-expanded={expanded}
                          aria-label="show more"
                        >
                          <ExpandMoreIcon />
                        </IconButton>
                      </CardActions>
                      <Collapse in={expanded} timeout="auto" unmountOnExit>
                        <CardContent>
                          <Typography paragraph>COMMENTS:</Typography>
                          <TextField/>
                          <Button variant="contained" color="primary">add comment</Button>
                          <Typography paragraph>
                             someones comment here
                          </Typography>
                         </CardContent>
                      </Collapse>
                     </Card>

                  )
                })
              }
          </Paper>
        </Grid>
        <Grid item xs={4}>
          <Paper className={classes.paper2}>
            <h3>Friends</h3>
            <Divider/>
            <div className='friendsOnline'>
            {
              props.friends.friendList.map(item=>{
                return(
                    
                      <div className='friendCard' key={item.id}>
                        <img src={`http://localhost:5000/${item.photo}`} width='100' heigth='100'/>
                        <p>{item.name} is online</p>
                      </div>
                )
              })
            }
            </div>
          </Paper>
        </Grid>
        
      </Grid>
    </div>

        </main>
        </div>
  )
};

export default connect(r=>r)(ProfileComponent);
