import React, { useState, useEffect } from 'react';
import '../../App.css'
import { connect } from 'react-redux';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import { loadProfile } from '../../redux/actions/user';
import '../profileComponents/profile.css'
import { useStyles } from '../profileComponents/profilestyles';
import { Paper, Grid } from '@material-ui/core';
import ToolBar from '../../parts/ToolBar';
import NavBar from '../../parts/adminNavbar';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { loadAllUsers, blockUser } from '../../redux/actions/adminAction';
import './admin.css' 
const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);

function createData(name, calories, fat, carbs, protein) {
  return { name, calories, fat, carbs, protein };
}

const rows = [
  createData('Frozen yoghurt', 159, 6.0, 24, 4.0),
  createData('Ice cream sandwich', 237, 9.0, 37, 4.3),
  createData('Eclair', 262, 16.0, 24, 6.0),
  createData('Cupcake', 305, 3.7, 67, 4.3),
  createData('Gingerbread', 356, 16.0, 49, 3.9),
];

const useStyles1 = makeStyles({
  table: {
    minWidth: 700,
  },
});



function Admin(props) {
   
          // const [load, setLoad] = useState(true)
         useEffect(()=>{
            // if(load == true){
                let key = sessionStorage.currentUser
                props.dispatch(loadProfile(key,props.history))
                props.dispatch(loadAllUsers())
                //  setLoad(false)
            //  }
        },[])

       
        // console.log(props);
    const classes1 = useStyles1();     
    const classes = useStyles()
   
     return (
        <div className={classes.root}>
        <CssBaseline />
           <ToolBar/>
         <NavBar/>
        <main className={classes.content}>
          <div className={classes.toolbar}></div>
           <div className={classes.root3}>
      <Grid container spacing={3}>
         <Grid item xs={12}>
         <TableContainer component={Paper}>
      <Table className={classes1.table} aria-label="customized table">
        <TableHead>
          <TableRow>
              
             <StyledTableCell align="center">ID</StyledTableCell>
             <StyledTableCell align="center">Name</StyledTableCell>
             <StyledTableCell align="center">Surname</StyledTableCell>
             <StyledTableCell align="center">Actions</StyledTableCell>
    
          </TableRow>
        </TableHead>
        <TableBody>
          {props.admin.usersforAdmin.map((item) => (
            <StyledTableRow key={item.id}>
              <StyledTableCell align="center">{item.id}</StyledTableCell>
              <StyledTableCell align="center">{item.name}</StyledTableCell>
              <StyledTableCell align="center">{item.surname}</StyledTableCell>
              <StyledTableCell align="center">
          <button className={item.status===0?'unblocked':'blocked'} onClick={(e)=>props.dispatch(blockUser(item.id,e.target))}>{item.status===0?'Block':'Unblock'}</button>
              </StyledTableCell>
            </StyledTableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
        </Grid>
       </Grid>
    </div>
        </main>
        </div>
  )
};

export default connect(r=>r)(Admin);
