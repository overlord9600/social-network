import React from 'react'
import { connect } from 'react-redux'
import {changeInput, userValidate} from '../../redux/actions/user'
import {Link} from 'react-router-dom';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Drawer from '@material-ui/core/Drawer';
import Hidden from '@material-ui/core/Hidden';
import IconButton from '@material-ui/core/IconButton';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import MenuIcon from '@material-ui/icons/Menu';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import LockOpenIcon from '@material-ui/icons/LockOpen';
import { useTheme } from '@material-ui/core/styles';
import logo from '../../logo.png'
import TextField from '@material-ui/core/TextField';
 import Button from '@material-ui/core/Button';
import InputAdornment from '@material-ui/core/InputAdornment';
import AccountCircle from '@material-ui/icons/AccountCircle';
import VpnKeyIcon from '@material-ui/icons/VpnKey';
import AlternateEmailIcon from '@material-ui/icons/AlternateEmail';
import { useStyles } from '../signcomponents/signstyle';

 
 



  
function Signup(props){
  const classes = useStyles();
   let user=props.user.signup
   let error=props.user.errorsup
 
   const { window } = props;
    const theme = useTheme();
   const [mobileOpen, setMobileOpen] = React.useState(false);
 
   const handleDrawerToggle = () => {
     setMobileOpen(!mobileOpen);
   };
 
   const drawer = (
     <div>
       <div className={classes.toolbar}>
         <img src={logo} width='40' height='50'/>
       </div>
        <List>
       <Link to='/' className={classes.links}>
       <ListItem button>
       <ListItemIcon>
         <LockOpenIcon/><ListItemText>Login</ListItemText>
         </ListItemIcon>
        </ListItem>
        </Link>
       <Link to='/signup' className={classes.links}>
       <ListItem button>
       <ListItemIcon>
         <LockOpenIcon/><ListItemText>Sign up</ListItemText>
         </ListItemIcon>
        </ListItem>
        </Link>
       </List>
        <List>
       </List>
     </div>
   );
 
   const container = window !== undefined ? () => window().document.body : undefined;
 
   return (
   <>
     {/* <Route path="/profile"  component={ProfileComponent}/> */}
     <div className={classes.root}>
       <CssBaseline />
       <AppBar position="fixed" style={{backgroundColor:'#212529'}} className={classes.appBar}>

         <Toolbar>
           <IconButton
             color="inherit"
             aria-label="open drawer"
             edge="start"
             onClick={handleDrawerToggle}
             className={classes.menuButton}
           >
             <MenuIcon />
           </IconButton>
           
           <Typography variant="h6" noWrap>
             Shield Network
           </Typography>
         </Toolbar>
       </AppBar>
       
        

        <nav className={classes.drawer} aria-label="mailbox folders">
         {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
         <Hidden smUp implementation="css">
           <Drawer
             container={container}
             variant="temporary"
             anchor={theme.direction === 'rtl' ? 'right' : 'left'}
             open={mobileOpen}
             onClose={handleDrawerToggle}
             classes={{
               paper: classes.drawerPaper,
             }}
             ModalProps={{
               keepMounted: true, // Better open performance on mobile.
             }}
           >
             {drawer}
           </Drawer>
         </Hidden>
         <Hidden xsDown implementation="css">
           <Drawer
             classes={{
               paper: classes.drawerPaper,
             }}
             variant="permanent"
             open
           >
             {drawer}
           </Drawer>
         </Hidden>
       </nav>
       <main className={classes.content}>
         <div className={classes.toolbar}></div>
         <>
         <div className='loginBox'>
        <h1>Sign Up</h1>

    <p style={props.user.errorsup.error.includes('successfully')?{color:'green'}:{color:'red'}}>{props.user.errorsup.error}</p>

      <form className={classes.root1} noValidate autoComplete="on">
        
      <div>
         <TextField
          error={props.user.errorsup.error===''||props.user.errorsup.error==='ok'?false:true}
          id="name"
          label="First name"
          defaultValue={user.name} 
          onChange={(e)=>props.dispatch(changeInput('name',e.target.value))} 
          helperText=""
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <AccountCircle />
              </InputAdornment>
            ),
          }}
        />
        <TextField
          error={props.user.errorsup.error===''||props.user.errorsup.error==='ok'?false:true}
          id="surname"
          label="Last name"
          defaultValue={user.surname} 
          onChange={(e)=>props.dispatch(changeInput('surname',e.target.value))} 
          helperText=""
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <AccountCircle />
              </InputAdornment>
            ),
          }}
        />
      </div>
      <div>
      <TextField
          error={props.user.errorsup.loginerror===''?false:true}
          id="login"
          label={props.user.errorsup.loglabelerror}
          defaultValue={user.login} 
          onChange={(e)=>props.dispatch(changeInput('login',e.target.value))} 
          helperText={props.user.errorsup.loginerror}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <AlternateEmailIcon />
              </InputAdornment>
            ),
          }}
        />
        <TextField
          error={props.user.errorsup.passerror===''?false:true}
          id="password"
          label={props.user.errorsup.passlabelerror} 
          defaultValue={user.password} 
          onChange={(e)=>props.dispatch(changeInput('password',e.target.value))} 
          helperText={props.user.errorsup.passerror}
          type="password"
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <VpnKeyIcon />
              </InputAdornment>
            ),
          }}
        />
      </div>
      <br/>
      <Button variant="contained" color="primary"  onClick={()=>props.dispatch(userValidate(props))}>Sign up</Button>
    </form>
    <p>
        Aleady have an account? <br />
        <Link to="/">Log in here</Link>
       </p>
    </div>
       </> 
       </main>
     </div>
     </>
)
}

export default connect(r=>r)(Signup)