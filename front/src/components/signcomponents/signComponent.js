import React from 'react'
import { connect } from 'react-redux'
import {changeInput1, userLogin} from '../../redux/actions/user'
import {Link} from 'react-router-dom';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Drawer from '@material-ui/core/Drawer';
import Hidden from '@material-ui/core/Hidden';
import IconButton from '@material-ui/core/IconButton';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import MenuIcon from '@material-ui/icons/Menu';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import LockOpenIcon from '@material-ui/icons/LockOpen';
import { useTheme } from '@material-ui/core/styles';
import logo from '../../logo.png'
import TextField from '@material-ui/core/TextField';
 import Button from '@material-ui/core/Button';
import InputAdornment from '@material-ui/core/InputAdornment';
import AccountCircle from '@material-ui/icons/AccountCircle';
import VpnKeyIcon from '@material-ui/icons/VpnKey';
import AlternateEmailIcon from '@material-ui/icons/AlternateEmail';
import { useStyles } from '../signcomponents/signstyle';
  

function Login(props){
  const classes = useStyles();
   let user=props.user.signin
   let error=props.user.errorsin
 
   const { window } = props;
    const theme = useTheme();
   const [mobileOpen, setMobileOpen] = React.useState(false);
 
   const handleDrawerToggle = () => {
     setMobileOpen(!mobileOpen);
   };
 
   const drawer = (
     <div>
       <div className={classes.toolbar}>
         <img src={logo} width='40' height='50'/>
       </div>
        <List>
       <Link to='/login' className={classes.links}>
       <ListItem button>
       <ListItemIcon>
         <LockOpenIcon/><ListItemText>Login</ListItemText>
         </ListItemIcon>
        </ListItem>
        </Link>
       <Link to='/signup' className={classes.links}>
       <ListItem button>
       <ListItemIcon>
         <LockOpenIcon/><ListItemText>Sign up</ListItemText>
         </ListItemIcon>
        </ListItem>
        </Link>
       </List>
        <List>
       </List>
     </div>
   );
 
   const container = window !== undefined ? () => window().document.body : undefined;
 
   return (
   <>
      <div className={classes.root}>
       <CssBaseline />
       <AppBar position="fixed" style={{backgroundColor:'#212529'}} className={classes.appBar}>

         <Toolbar>
           <IconButton
             color="inherit"
             aria-label="open drawer"
             edge="start"
             onClick={handleDrawerToggle}
             className={classes.menuButton}
           >
             <MenuIcon />
           </IconButton>
           
           <Typography variant="h6" noWrap>
             Shield Network
           </Typography>
         </Toolbar>
       </AppBar>
       
        

        <nav className={classes.drawer} aria-label="mailbox folders">
         {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
         <Hidden smUp implementation="css">
           <Drawer
             container={container}
             variant="temporary"
             anchor={theme.direction === 'rtl' ? 'right' : 'left'}
             open={mobileOpen}
             onClose={handleDrawerToggle}
             classes={{
               paper: classes.drawerPaper,
             }}
             ModalProps={{
               keepMounted: true, // Better open performance on mobile.
             }}
           >
             {drawer}
           </Drawer>
         </Hidden>
         <Hidden xsDown implementation="css">
           <Drawer
             classes={{
               paper: classes.drawerPaper,
             }}
             variant="permanent"
             open
           >
             {drawer}
           </Drawer>
         </Hidden>
       </nav>
       <main className={classes.content}>
         <div className={classes.toolbar}></div>
         <div className='loginBox'>
       <h1>Login</h1>

   <p style={error.error=='ok'?{color:'green'}:{color:'red'}}>{error.error}</p>

     <form className={classes.root1} noValidate autoComplete="on">
         
     <div>
     <TextField
         error={error.error.includes('wrong')||error.error.includes('login')?true:false}
         id="login"
         label={error.loglabelerror}
         defaultValue={user.login} 
         onChange={(e)=>props.dispatch(changeInput1('login',e.target.value))} 
         helperText={error.loginerror}
         InputProps={{
           startAdornment: (
             <InputAdornment position="start">
               <AlternateEmailIcon />
             </InputAdornment>
           ),
         }}
       />
       </div>
       <div>
       <TextField
         error={error.error.includes('wrong')||error.error.includes('password')?true:false} 
         id="password"
         label={error.passlabelerror} 
         defaultValue={user.password} 
         onChange={(e)=>props.dispatch(changeInput1('password',e.target.value))} 
         helperText={error.passerror}
         type="password"
         InputProps={{
           startAdornment: (
             <InputAdornment position="start">
               <VpnKeyIcon />
             </InputAdornment>
           ),
         }}
       />
     </div>
     <br/>
     <Button variant="contained" color="primary"  onClick={()=>props.dispatch(userLogin(props))}>Log in</Button>
   </form>
   <hr/>
    <Link variant="contained" to='/signup'>CREATE NEW ACCOUNT</Link>
   </div>
           
       </main> 
     </div>
     </>
)
}

export default connect(r=>r)(Login)