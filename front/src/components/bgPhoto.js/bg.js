import React, { useEffect } from 'react'
import { connect } from 'react-redux';
import { loadTheme } from '../../redux/actions/user';
import { useStyles } from '../profileComponents/profilestyles';


 function Bg(props){
    const classes = useStyles();

    useEffect(()=>{
        props.dispatch(loadTheme())
    },[])

    return <img className={classes.bgImg} src={`http://localhost:5000/${props.user.bgPhoto.data}`} width='100%' height='100%'/>
           
}

export default connect(r=>r)(Bg)