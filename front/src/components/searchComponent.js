import React, { useState, useEffect } from 'react';
import '../App.css'
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import IconButton from '@material-ui/core/IconButton'; 
import MenuIcon from '@material-ui/icons/Menu';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import logo from '../logo.png'
import { loadProfile } from '../redux/actions/user';
import SearchIcon from '@material-ui/icons/Search';
import './profileComponents/profile.css'
import { useStyles } from './profileComponents/profilestyles';
import { LogOut } from '../redux/actions/user';
import { updateFriends, loadRequests, searchInputChange, goToProfile, addToFriendsReq } from '../redux/actions/friends';
import { Paper, FormControl, InputLabel, Input, InputAdornment, Button } from '@material-ui/core';
import { loadPosts } from '../redux/actions/photosActions';
import NavBar from '../parts/NavBar';


 
  
  const useStyles2 = makeStyles((theme) => ({
    margin: {
      margin: theme.spacing(1),
      width:100+'%',
      marginBottom:50
    },
    root: {
      maxWidth: 270,
      margin:25
    },
  }));

function Search(props) {
    const classes2 = useStyles2();
         const [load, setLoad] = useState(true)

        useEffect(()=>{
            if(load == true){
                let key = sessionStorage.currentUser
                props.dispatch(loadProfile(key,props.history))
                props.dispatch(loadPosts())
                props.dispatch(updateFriends())
                props.dispatch(loadRequests())

                setLoad(false)
             }
        })
 
        const { window } = props;
    const classes = useStyles();
     const [mobileOpen, setMobileOpen] = React.useState(false);
     const handleDrawerToggle = () => {
      setMobileOpen(!mobileOpen);
    };
     return (
        <div className={classes.root}>
        <CssBaseline />
        <AppBar position="fixed" style={{backgroundColor:'#212529'}} className={classes.appBar}>
               <Toolbar>
                 
            <IconButton
              color="inherit"
              aria-label="open drawer"
              edge="start"
              onClick={handleDrawerToggle}
              className={classes.menuButton}
            >
              <MenuIcon />
            </IconButton>
            
            <img src={logo} width='40' height='50'/>
            <Typography variant="h6" noWrap>
              Shield Network
            </Typography>
            <div style={{display:'flex'}}>
            <Link style={{color:'white'}} to="/"
             onClick={()=>props.dispatch(LogOut())}
             >Log out</Link>
            </div>
             

          </Toolbar>
        </AppBar>
      <NavBar/>
        <main className={classes.content}>
          <div className={classes.toolbar}></div>
          <FormControl className={classes2.margin}>
                      <InputLabel htmlFor="input-with-icon-adornment">SEARCH FOR FRIENDS...</InputLabel>
                      <Input
                        id="input-with-icon-adornment"
                        value={props.friends.searchText}
                        onChange={(e)=>props.dispatch(searchInputChange('searchedUsers',e.target.value))}
                        startAdornment={
                          <InputAdornment position="start">
                            <SearchIcon />
                          </InputAdornment>
                        }
                      />
                    </FormControl>
          
                        {

                            props.friends.searchedUsers.map(item=>{
                                return(
                                  <Paper elevation={3}  key={item.id}>
                                    <div className='friendCard'>
                                        <div className='friendImg'>
                                        <h3>{item.name} {item.surname}</h3>
                                            <img src={`http://localhost:5000/${item.photo}`} height='100' width='100'/>
                                            
                                        </div>
                                        <div className='actions'>
                                        <Button 
                                        size="small" 
                                        color="primary"
                                        // style={props.friends.friendList.includes(item)?{display:'none'}:{display:'block'}}
                                        onClick={()=>props.dispatch(addToFriendsReq(item.id))}>
                                          Add friend
                                        </Button>
                                        <Button    
                                        size="small" 
                                        color="primary"
                                        onClick={()=>props.dispatch(goToProfile(item.id,props.history))}
                                        >
                                          profile
                                        </Button>
                                        </div>
                                    </div>
                                 </Paper>
                                )
                            })
                        }


        </main>
        </div>
  )
};

export default connect(r=>r)(Search);
