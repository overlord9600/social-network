import React, { useState, useEffect } from 'react';
import '../App.css'
import { connect } from 'react-redux';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import './profileComponents/profile.css'
import { useStyles } from './profileComponents/profilestyles';
import { loadProfile, changeInputInLoginChanges, checkLogin, upload, checkPasswords, changeInputInPasswordChanges, LogOut } from '../redux/actions/user';
import { Paper, TextField, Button } from '@material-ui/core';
import AddIcon from "@material-ui/icons/Add";
import { Fab } from "@material-ui/core";
import ToolBar from '../parts/ToolBar';
import NavBar from '../parts/NavBar';
 


function Settings(props) {

          const [load, setLoad] = useState(true)

        useEffect(()=>{
            if(load == true){
                let key = sessionStorage.currentUser
                props.dispatch(loadProfile(key,props.history))
                setLoad(false)
             }
        })

     const classes = useStyles();
    
 //login
 let loginch=props.user.changeLogin
 let logincherror=props.user.errorChangeLogin
  //password
 let password = props.user.changePassword
 let paswordcherror = props.user.errorChangePassword
    return (
        <div className={classes.root}>
        <CssBaseline />
        <ToolBar/>
         <NavBar/>
        <main className={classes.content}>
          <div className={classes.toolbar}></div>
          <Paper elevation={3}>
               <div>
         
            <div id='settchange'>
              <h3>Change Login</h3>
              <p>{logincherror.error}</p>
         <TextField
            id='newLogin' 
            label={logincherror.loglabelerror}
            defaultValue={loginch.newLogin}
            helperText={logincherror.loginerror}
            onChange={(e)=>props.dispatch(changeInputInLoginChanges('newLogin',e.target.value))}
          />
            
         <TextField
            id='password'
            type="password"
            label={logincherror.passlabelerror}
             helperText={logincherror.loginerror}
            defaultValue={loginch.password}
            onChange={(e)=>props.dispatch(changeInputInLoginChanges('password',e.target.value))}
          />
                <Button variant="contained" color="primary" onClick={()=>props.dispatch(checkLogin(loginch))}>Change</Button>

            </div>
              

        </div>
   
          </Paper>
          <Paper elevation={3}>
               <div>
 
          <div id='settchange'>
          <h3>Change password</h3>
          <p>{paswordcherror.error}</p>
          
            <TextField
              id='oldPassword'
              type='password'
              label={paswordcherror.oldPasslabelerror}
              defaultValue={password.oldPassword}
              onChange={(e)=>props.dispatch(changeInputInPasswordChanges('oldPassword',e.target.value))}
             />
         
            <TextField
              id='newPassword'
              type='password'
              label={paswordcherror.newPasslabelerror}
              defaultValue={password.newPassword}
              onChange={(e)=>props.dispatch(changeInputInPasswordChanges('newPassword',e.target.value))}
             />
           <Button variant="contained" color="primary" onClick={()=>props.dispatch(checkPasswords(password))}>Change</Button>

          </div>
          
        </div>

          </Paper>
          <Paper elevation={3} id='settchange'>
               <div>
                
        <form method='post' id='myForm' encType='multipart/form-data'>
         
        <input
          // style={{ display: "none" }}
          id="nkar"
          name="nkar"
          type="file"
        />
        <Fab
          color="secondary"
          size="small"
          component="span"
          aria-label="add"
          variant="extended"
          onClick={()=>props.dispatch(upload(props.history))}
        >
         
          <AddIcon   /> Upload profile photo
        </Fab>
           
          </form>
        </div>
          </Paper>
          <Paper>
          <div>
           <div id='settchange'>
            <h3>Visability for other users</h3>
            <input type='checkbox'/>Private
            <input type='checkbox'/>Public
            </div>
            </div>
          </Paper>


        </main>
        </div>
  )
};

export default connect(r=>r)(Settings);
