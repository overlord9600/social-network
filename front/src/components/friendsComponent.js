import React, { useState, useEffect } from 'react';
import '../App.css'
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import './profileComponents/profile.css'
import { useStyles } from './profileComponents/profilestyles';
import { loadProfile } from '../redux/actions/user';
import { Paper, Button, Card, CardActionArea, CardMedia, CardContent, CardActions } from '@material-ui/core';
import { updateFriends, deletaFromFriends, goToProfile } from '../redux/actions/friends';
import ToolBar from '../parts/ToolBar';
import NavBar from '../parts/NavBar';
 


function FriendComponent(props) {

        useEffect(()=>{
          
                let key = sessionStorage.currentUser
                props.dispatch(loadProfile(key,props.history))
                props.dispatch(updateFriends())
        },[])
    const classes = useStyles();
    
    return (
        <div className={classes.root}>
        <CssBaseline />
        <ToolBar/>
   
         <NavBar/>
          <main className={classes.content}>
          <div className={classes.toolbar}></div>
          <h4>{props.friends.friendList.length===0?"You don't have any friends":`You have ${props.friends.friendList.length} friends`}</h4>
              <h4 style={props.friends.friendList.length===0?{display:'block'}:{display:'none'}}>You can search for friends <Link to='/profile/search'>here</Link></h4>
            <Paper elevation={3} className='friendlist'>
              {
                props.friends.friendList.map((item,index)=>{
                  return(
                  <Card className={classes.root2} key={item.id}>
                      <CardActionArea>
                        <CardMedia
                          component="img"
                          alt="Contemplative Reptile"
                          height="140"
                          image={`http://localhost:5000/${item.photo}`}
                          title="Contemplative Reptile"
                        />
                        <CardContent>
                          <Typography gutterBottom variant="h5" component="h2">
                            {item.name} {item.surname}
                          </Typography>
                           
                        </CardContent>
                      </CardActionArea>
                      <CardActions>
                        <Button onClick={()=>props.dispatch(deletaFromFriends(item.id,index,props.friends.friendList))} size="small" color="primary">
                          delete
                        </Button>
                        <Button onClick={()=>props.dispatch(goToProfile())} size="small" color="primary">
                          see profile
                        </Button>
                      </CardActions>
                    </Card>
                  )
                })
              }
            
            </Paper>
         </main>
        </div>
  )
};

export default connect(r=>r)(FriendComponent);
