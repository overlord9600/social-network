import React from 'react';
import { connect } from 'react-redux';
import socketIOClient from "socket.io-client";
import { changeInputOfMessage, sendMessage } from '../../redux/actions/chat';
import './Compose.css';
const ENDPOINT = "http://127.0.0.1:5000";
const socket = socketIOClient(ENDPOINT);


 function Compose(props) {
    return (
      <div className="compose">
        <input
          type="text"
          className="compose-input"
          placeholder="Type a message, @name"
          value={props.chat.text}
        onChange={(e)=>props.dispatch(changeInputOfMessage(e.target.value))}
        onKeyDown={(e)=>props.dispatch(sendMessage(e.key,socket))}
        />
      </div>
    );
}

export default connect(r=>r)(Compose)