import React, { useState, useEffect } from 'react';
import '../App.css'
import { connect } from 'react-redux';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline'; 
import Typography from '@material-ui/core/Typography';
import './profileComponents/profile.css'
import { useStyles } from './profileComponents/profilestyles';
import { loadProfile, loadTheme } from '../redux/actions/user';
import { Paper, Button, Card, CardActionArea, CardMedia, CardContent, CardActions } from '@material-ui/core';
import { loadRequests, deletaFromReq, addToFriendList, updateFriends, showRequests } from '../redux/actions/friends';
import ToolBar from '../parts/ToolBar';
import NavBar from '../parts/NavBar';
import { loadPhotos } from '../redux/actions/photosActions';
 
function RequestComponent(props) {

         const [load, setLoad] = useState(true)

        useEffect(()=>{
          let key = sessionStorage.currentUser

            async function fetchData(){
              await  props.dispatch(loadProfile(key,props.history))
              props.dispatch(loadRequests())
            }
            fetchData()
        },[])
      
     const classes = useStyles();
    
    return (
        <div className={classes.root}>
        <CssBaseline />
       <ToolBar/>
          <NavBar/>
          <main className={classes.content}>
          <div className={classes.toolbar}></div>
            <h4>{props.friends.requests.length===0?'There are no requests':`You have ${props.friends.requests.length} requests`}</h4>
            <Paper elevation={3} className='friendlist'>
              {
                props.friends.requests.map((item,index)=>{
                  return(
                  <Card className={classes.root2} key={item.id}>
                      <CardActionArea>
                        <CardMedia
                          component="img"
                          alt="Contemplative Reptile"
                          height="140"
                          image={`http://localhost:5000/${item.photo}`}
                          title="Contemplative Reptile"
                        />
                        <CardContent>
                          <Typography gutterBottom variant="h5" component="h2">
                            {item.name} {item.surname}
                          </Typography>
                           
                        </CardContent>
                      </CardActionArea>
                      <CardActions>
                        <Button onClick={()=>props.dispatch(deletaFromReq(index))} size="small" color="primary">
                          decline
                        </Button>
                        <Button onClick={()=>props.dispatch(addToFriendList(item.id,index,props.friends.requests))} size="small" color="primary">
                          add to friends
                        </Button>
                      </CardActions>
                    </Card>
                  )
                })
              }
            
            </Paper>
         </main>
        </div>
  )
};

export default connect(r=>r)(RequestComponent);
