import React, { useState, useEffect } from 'react';
import './messanger.css'
 

import { connect } from 'react-redux';
import CssBaseline from '@material-ui/core/CssBaseline'; 
import Typography from '@material-ui/core/Typography';
import '../profileComponents/profile.css'
import { useStyles } from '../profileComponents/profilestyles';
import { loadProfile } from '../../redux/actions/user';
import ToolBar from '../../parts/ToolBar';
import NavBar from '../../parts/NavBar';
import { showMessages,startChat, sendMessage, changeInput, freandsChatList, loadMessages } from '../../redux/actions/chat';
 

import Toolbar from '../Toolbar';
import ToolbarButton from '../ToolbarButton';
import ConversationSearch from '../ConversationSearch';
import Compose from '../Compose';
import socketIOClient from "socket.io-client";

 const ENDPOINT = "http://127.0.0.1:5000";
 const socket = socketIOClient(ENDPOINT);

function MessangerComponent(props) {
        useEffect(()=>{
                let key = sessionStorage.currentUser
                props.dispatch(loadProfile(key,props.history))
                props.dispatch(freandsChatList())
                socket.on('getMessages',(data)=> props.dispatch(showMessages(data)))
                socket.on('newMessage',(data)=> props.dispatch(showMessages(data)))
                // socket.on('newMessage',(data)=> setTest(data))
        },[])
      const classes = useStyles();
      // console.log(props);

    return (
        <div className={classes.root}>
        <CssBaseline />
         
      <ToolBar/>
          
         <NavBar/>
          <main className={classes.content}>
          <div className={classes.toolbar}></div>

          <div className="messenger">
          
            <div className="scrollable sidebar">
            <Toolbar
          title="Messenger"
          leftItems={[
            <ToolbarButton key="cog" icon="ion-ios-cog" />
          ]}
          rightItems={[
            <ToolbarButton key="add" icon="ion-ios-add-circle-outline" />
          ]}
        />
        <ConversationSearch />
        {
          props.chat.frends.map((item,index)=>{
            let style = item.id == props.chat.currentUser ? {backgroundColor:"green"} : {backgroundColor:"white"}
            return(
                <div className="conversation-list-item" style={style} key={item.id} onClick={()=>props.dispatch(startChat(item.id,socket))}>
                  <img className="conversation-photo" src={`http://localhost:5000/${item.photo}`} alt="conversation" />
                  <div className="conversation-info">
                    <h1 className="conversation-title">{item.name} {item.surname}</h1>
                    {/* <p className="conversation-snippet">sdfd</p> */}
                  </div>
                </div>
            )
          })
        }
            
            </div>

            <div className="scrollable content">
            <Toolbar
          // title={}
          rightItems={[
            <ToolbarButton key="info" icon="ion-ios-information-circle-outline" />,
            <ToolbarButton key="video" icon="ion-ios-videocam" />,
            <ToolbarButton key="phone" icon="ion-ios-call" />
          ]}
        />
        <div className="message-list-container">
          {
            props.chat.messenger.map((item,index)=>{
              let style = item.user1_id == props.chat.currentUser?"replies" : "sent"
              return(
                <div className={style} key={item.id}>
                <div className="bubble-container">
                <div className="bubble">
                  {item.text}
                </div>
              </div>
              </div>
              )
            })
          }
        </div>
        <Compose rightItems={[
          <ToolbarButton key="photo" icon="ion-ios-camera" />,
          <ToolbarButton key="image" icon="ion-ios-image" />,
          <ToolbarButton key="audio" icon="ion-ios-mic" />,
          <ToolbarButton key="money" icon="ion-ios-card" />,
          <ToolbarButton key="games" icon="ion-logo-game-controller-b" />,
          <ToolbarButton key="emoji" icon="ion-ios-happy" />
        ]}/>
            </div>
          </div>

         </main>
        </div>
  )
};

export default connect(r=>r)(MessangerComponent);
