import React, { useState, useEffect } from 'react';
import '../App.css'
import { connect } from 'react-redux';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import './profileComponents/profile.css'
import { useStyles } from './profileComponents/profilestyles';
import { loadProfile } from '../redux/actions/user';
import { Paper } from '@material-ui/core';
import {changePostText, addPost, loadPosts, photoChange} from '../redux/actions/photosActions'
import Slider from '../parts/slider';
import ToolBar from '../parts/ToolBar';
import NavBar from '../parts/NavBar';
 
 
 


function PhotoComponent(props) {
// console.log(props);
         const [load, setLoad] = useState(true)
        
        useEffect(()=>{
            if(load == true){
                let key = sessionStorage.currentUser
                props.dispatch(loadProfile(key,props.history))
                props.dispatch(loadPosts())
                
                setLoad(false)
             }
        })
    const { window } = props;
    const classes = useStyles();
     const [mobileOpen, setMobileOpen] = React.useState(false);
   
    return (
        <div className={classes.root}>
        <CssBaseline />
           <ToolBar/>       
          <NavBar/>
          <main className={classes.content}>
          <div className={classes.toolbar}></div>
             {
                props.photos.photo.length===0?(
                    <h4>There are no photos <br/>
                    <div>
                    <div className={classes.paper}>
            <Typography>Upload photo</Typography>
          <form method='post' id='myPost' encType='multipart/form-data'>
            <input type='file' name='postnkar'/>
            <br/>
            <textarea 
            id='postText'
            placeholder='comment your photo'
            value={props.photos.postText}
            onChange={(e)=>props.dispatch(changePostText('postText',e.target.value))}
           
            ></textarea>
            <br/>

            <button  onClick={()=>props.dispatch(addPost())} type='button'>save</button>
          </form>
          </div>
        
      
    </div>
          
                    </h4>
                ):(
                <h4>There are {props.photos.photo.length} photos <br/>
                 <div>
       
      
          <div className={classes.paper}>
            <Typography>Upload photo</Typography>
          <form method='post' id='myPost' encType='multipart/form-data'>
            <input type='file' name='postnkar'/>
            <br/>
            <textarea 
            id='postText'
            placeholder='comment your photo'
            value={props.photos.photo.postText}
            onChange={(e)=>props.dispatch(changePostText('postText',e.target.value))}
           
            ></textarea>
            <br/>

            <button  onClick={()=>props.dispatch(addPost(props.photos.photo.postText))} type='button'>save</button>
          </form>
          </div>
        
    </div>
                </h4>
                )
            }
            <Paper elevation={3} className='friendlist'>
              {
                props.photos.photo.map((item,index)=>{
                  return(
                    <div className="demo photo" onClick={()=>props.dispatch(photoChange(item.photo))} key={index}>
                      <div className="card-content">
                        <img src={`http://localhost:5000/${item.photo}`} alt="" className="bg" />
                        <div className="bg-object"></div>
                        <div className="title">{item.text}</div>  
                      </div>
                    </div>
                  )
                })
              }
            <Slider/>
            
            </Paper>
         </main>
        </div>
  )
};

export default connect(r=>r)(PhotoComponent);
